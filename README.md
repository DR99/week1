# week1

## Lesson November 7th, 2022

1. Module 2 introduction: [pdf](./slides/Machine_learning_systems_for_data_science.pdf)
2. Jupyter Notebook: [pdf]](./slides/JupyterNotebook.pdf)
3. Markdown cell: [pdf](./slides/Markdownlanguage.pdf)
 - Markdown example: [notebook](./scripts/Markdown_cell.ipynb)
4. Python Libraries: [pdf](./slides/Libraries.pdf)
5. Check python libraries' versions: [pdf](./scripts/check_python_modules.ipynb)
6. Setup script: [notebook](./scripts/setup.ipynb)

## Lesson November 9th, 2022

1. pandas series: [notebook](./scripts/series_pandas.ipynb) and [pdf](./slides/series_pandas.pdf)
1. pandas dataframe: [notebook](./scripts/dataframe_pandas.ipynb) and [pdf](./slides/dataframe_pandas.pdf)

## Lesson November 10th, 2022

1. matplotlib library: [notebook](./scripts/matplotlib_library.ipynb) and [pdf](./slides/matplotlib_library.pdf)

Application of previous libraries to a set of use cases:

2. ISTAT data: [notebook](./use_cases/ISTAT_data.ipynb)
3. Italian COVID 19 data: [notebook](./use_cases/covid_data.ipynb)
4. top us cities: [notebook](./use_cases/top_us_cities_data.ipynb)
